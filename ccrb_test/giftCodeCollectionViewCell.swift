//
//  giftCodeCollectionViewCell.swift
//  CCRBX
//
//  Created by Beeone on 27/07/18.
//  Copyright © 2018 Beeone. All rights reserved.
//

import UIKit

class giftCodeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgGiftCode: UIImageView!
}
